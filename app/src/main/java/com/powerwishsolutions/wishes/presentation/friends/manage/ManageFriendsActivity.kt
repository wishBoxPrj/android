package com.powerwishsolutions.wishes.presentation.friends.manage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.presentation.common.pager.ManageFriendsFragmentAdapter
import com.powerwishsolutions.wishes.utils.common.RESULT_FRIENDS_UPDATED
import kotlinx.android.synthetic.main.activity_manage_friends.*
import org.koin.android.viewmodel.ext.android.viewModel

class ManageFriendsActivity : AppCompatActivity() {

    val vm: ManageFriendsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_friends)

        manage_friends_pager.adapter = ManageFriendsFragmentAdapter(this, supportFragmentManager)
        manage_friends_tabs.setViewPager(manage_friends_pager)

        manage_friends_back.setOnClickListener { finish() }

        vm.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, getString(it), Toast.LENGTH_SHORT).show()
                vm.clearErrorMessage()
            }
        })
    }

    override fun finish() {
        if (vm.hasResult) { setResult(RESULT_FRIENDS_UPDATED) }
        super.finish()
    }
}
