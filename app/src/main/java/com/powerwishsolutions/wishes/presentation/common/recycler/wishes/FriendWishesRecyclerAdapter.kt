package com.powerwishsolutions.wishes.presentation.common.recycler.wishes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.extensions.setAvatar
import com.powerwishsolutions.wishes.presentation.common.view.SvgRatingBar
import com.powerwishsolutions.wishes.utils.common.DateUtil
import kotlinx.android.synthetic.main.item_friend_profile.view.*
import kotlinx.android.synthetic.main.item_friend_wish.view.*

class FriendWishesRecyclerAdapter(
    private val context: Context?,
    private val profile: Profile?,
    private val list: ArrayList<Wish>,
    val listener: (Int) -> Unit,
    emptyListener: (empty: Boolean) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val PROFILE = 1

    init { emptyListener(list.size == 0) }

    override fun onCreateViewHolder(viewGroup: ViewGroup, type: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        return if (type == PROFILE) {
            val itemView = inflater.inflate(R.layout.item_friend_profile, viewGroup, false)
            FriendProfileViewHolder(itemView)
        } else {
            val itemView = inflater.inflate(R.layout.item_friend_wish, viewGroup, false)
            FriendWishViewHolder(itemView)
        }
    }

    override fun getItemCount(): Int {
        return list.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FriendWishViewHolder) {
            val listPosition = position - 1

            if (list[listPosition].booked_by != null) {
                setHolderItemsVisibility(holder, View.INVISIBLE, View.VISIBLE)
                holder.itemView.setOnClickListener(null)
            } else {
                setHolderItemsVisibility(holder, View.VISIBLE, View.GONE)
                holder.name.text = list[listPosition].name
                holder.comment.text = list[listPosition].comment
                holder.comment.isVisible = !list[listPosition].comment.isNullOrEmpty()
                holder.itemView.setOnClickListener { listener(listPosition) }
                list[listPosition].priority?.toFloat()?.let { holder.priority.rating = it }
            }
        } else if (holder is FriendProfileViewHolder) {
            holder.name.text = context?.getString(R.string.placeholder_name, profile?.owner?.first_name, profile?.owner?.last_name)
            holder.birthDate.text = DateUtil.getAppDateFormat(profile?.date_of_birth)
            holder.avatar.setAvatar(context, holder.letter, profile)
        }
    }

    class FriendWishViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.item_friend_wish_name
        val comment: TextView = itemView.item_friend_wish_comment
        val priority: SvgRatingBar = itemView.item_friend_wish_priority
        val bookedMessage: TextView = itemView.item_friend_wish_booked_message
        val bookedIcon: ImageView = itemView.item_friend_wish_booked_icon
    }

    class FriendProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.item_profile_name
        val birthDate: TextView = itemView.item_profile_birth_date
        val avatar: ImageView = itemView.item_profile_avatar
        val letter: TextView = itemView.item_profile_letter
    }

    private fun setHolderItemsVisibility(holder: FriendWishViewHolder, normal: Int, booked: Int) {
        holder.bookedIcon.visibility = booked
        holder.bookedMessage.visibility = booked
        holder.name.visibility = normal
        holder.comment.visibility = normal
        holder.priority.visibility = normal
    }

    fun markAsBooked(wish: Wish) {
        if (list.contains(wish)) {
            val id = list.indexOf(wish)
            notifyItemChanged(id + 1)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) return PROFILE
        return super.getItemViewType(position)
    }
}
