package com.powerwishsolutions.wishes.presentation.wishlist.create

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.domain.wishlist.repository.WishRepository

class WishCreateViewModel(private val repository: WishRepository) : ViewModel() {

    val category = MutableLiveData<String?>()
    val priority = MutableLiveData<Int>()
    val createdData = MutableLiveData<Wish>()
    val errorMessage = MutableLiveData<Int>()

    init {
        category.value = "undefined"
        priority.value = 1
    }

    fun setCategory(category: String?) {
        this.category.postValue(category)
    }

    fun createWish(name: String, comment: String) {
        var commentForPost: String? = null
        if (comment.isNotEmpty()) commentForPost = comment
        val wish = Wish(
            name = name,
            comment = commentForPost,
            category = category.value,
            priority = priority.value
        )
        repository.createWish(wish, {
            createdData.postValue(it)
        }, {
            errorMessage.postValue(R.string.error_creating_wish)
        })
    }

    fun clearErrorMessage() {
        errorMessage.value = null
    }

    fun setPriority(value: Int) {
        priority.postValue(value)
    }
}
