package com.powerwishsolutions.wishes.presentation.auth.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.presentation.auth.AuthState
import com.powerwishsolutions.wishes.presentation.auth.AuthViewModel
import com.powerwishsolutions.wishes.presentation.auth.web.WebAuthActivity
import kotlinx.android.synthetic.main.fragment_auth.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class AuthFragment : Fragment() {

    private val vm: AuthViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_auth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth_vk.setOnClickListener {
            startActivityForResult(Intent(context!!, WebAuthActivity::class.java), 1100)
        }

        vm.state.observe(this, Observer {
            when (it) {
                is AuthState.Initial -> auth_vk.isEnabled = true
                is AuthState.Authorizing -> auth_vk.isEnabled = false
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            21 -> if (data != null) vm.authWithVk(data.getStringExtra("code"))
            31 -> Toast.makeText(context, getString(R.string.error_auth_vk), Toast.LENGTH_SHORT).show()
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
