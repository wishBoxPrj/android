package com.powerwishsolutions.wishes.presentation.friends.wishlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.friends.repository.FriendsRepository
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.domain.wishlist.repository.WishRepository
import org.greenrobot.eventbus.EventBus

class FriendWishListViewModel(
    private val wishRepository: WishRepository,
    private val friendsRepository: FriendsRepository
) : ViewModel() {

    val state = MutableLiveData<FriendWishListState>()
    val bookedWish = MutableLiveData<Wish>()
    val friendDeleted = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<Int>()

    init {
        state.value = FriendWishListState.Initial
    }

    fun loadWishList(nickname: String?) {
        state.postValue(FriendWishListState.Loading)
        wishRepository.getFriendWishList(nickname, {
            state.postValue(FriendWishListState.Success(it))
        }, {
            state.postValue(FriendWishListState.Error)
        })
    }

    fun bookWish(wish: Wish?) {
        wishRepository.bookWish(wish, {
            EventBus.getDefault().post(it)
            bookedWish.postValue(it)
        }, {
            errorMessage.postValue(R.string.error_booking_wish)
        })
    }

    fun deleteFriend(friend: Profile) {
        friendsRepository.deleteFriend(friend, {
            EventBus.getDefault().post(it)
            friendDeleted.postValue(true)
        }, {
            errorMessage.postValue(R.string.error_friend_delete)
        })
    }

    fun clearErrorMessage() {
        errorMessage.value = null
    }
}
