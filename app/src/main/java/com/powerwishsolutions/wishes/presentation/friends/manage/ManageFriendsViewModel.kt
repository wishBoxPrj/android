package com.powerwishsolutions.wishes.presentation.friends.manage

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.friends.repository.FriendsRepository
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.presentation.friends.manage.requests.RequestsState
import com.powerwishsolutions.wishes.presentation.friends.manage.search.SearchFriendsState

class ManageFriendsViewModel(private val repository: FriendsRepository) : ViewModel() {

    val searchState = MutableLiveData<SearchFriendsState>()
    val requestState = MutableLiveData<RequestsState>()
    val requestedFriends = MutableLiveData<ArrayList<Profile>>()
    val processedRequest = MutableLiveData<Profile>()

    val errorMessage = MutableLiveData<Int>()
    private val latestQuery = MutableLiveData<String>()
    var hasResult = false

    init {
        searchState.value = SearchFriendsState.Initial
        requestState.value = RequestsState.Initial
        requestedFriends.value = ArrayList()
    }

    fun addRequestedFriend(friend: Profile) {
        repository.sendRequest(friend, {
            requestedFriends.value?.add(it)
            requestedFriends.postValue(requestedFriends.value)
        }, {
            errorMessage.postValue(R.string.error_sending_request)
        })
    }

    fun acceptRequest(friend: Profile) {
        repository.acceptRequest(friend, {
            hasResult = true
            processedRequest.postValue(friend)
        }, {
            errorMessage.postValue(R.string.error_accepting_request)
        })
    }

    fun denyRequest(friend: Profile) {
        repository.denyRequest(friend, {
            processedRequest.postValue(friend)
        }, {
            errorMessage.postValue(R.string.error_deny_request)
        })
    }

    fun searchFriends(query: String) {
        latestQuery.postValue(query)
        searchState.postValue(SearchFriendsState.Loading)
        repository.searchFriends(query, {
            searchState.postValue(SearchFriendsState.Success(it))
        }, {
            searchState.postValue(SearchFriendsState.Error)
        })
    }

    fun retrySearch() = latestQuery.value?.let { searchFriends(it) }

    fun getIncomingRequests(refresh: Boolean) {
        requestState.postValue(RequestsState.Loading(refresh))
        repository.getIncomingRequests({
            requestState.postValue(RequestsState.Success(it))
        }, {
            requestState.postValue(RequestsState.Error)
        })
    }

    fun clearErrorMessage() {
        errorMessage.value = null
    }
}
