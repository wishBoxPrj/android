package com.powerwishsolutions.wishes.presentation.auth

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.presentation.common.pager.AuthFragmentAdapter
import com.powerwishsolutions.wishes.presentation.main.MainActivity
import kotlinx.android.synthetic.main.activity_auth.*
import org.koin.android.viewmodel.ext.android.viewModel

class AuthActivity : AppCompatActivity() {

    private val vm: AuthViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = Color.TRANSPARENT
        setContentView(R.layout.activity_auth)
        auth_view_pager.adapter = AuthFragmentAdapter(supportFragmentManager)

        vm.state.observe(this, Observer {
            when (it) {
                is AuthState.NeedAddictionData -> {
                    auth_view_pager.currentItem = 1
                }
                is AuthState.Authorized -> {
                    startActivity(Intent(this, MainActivity::class.java))
                    this.finish()
                }
            }
        })

        vm.error.observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, getString(it), Toast.LENGTH_SHORT).show()
                vm.error.postValue(null)
            }
        })
    }
}
