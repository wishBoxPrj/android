package com.powerwishsolutions.wishes.presentation.wishlist.edit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Categories
import com.powerwishsolutions.wishes.extensions.finishWithResultAndData
import com.powerwishsolutions.wishes.presentation.common.dialog.category.CategoryChooseDialog
import com.powerwishsolutions.wishes.presentation.common.dialog.confirmation.ConfirmationDialog
import com.powerwishsolutions.wishes.utils.callback.CategorySelection
import com.powerwishsolutions.wishes.utils.callback.Confirmation
import com.powerwishsolutions.wishes.utils.common.RESULT_WISH_DELETED
import com.powerwishsolutions.wishes.utils.common.RESULT_WISH_EDITED
import kotlinx.android.synthetic.main.activity_wish_edit.*
import org.koin.android.viewmodel.ext.android.viewModel

class WishEditActivity : AppCompatActivity(), CategorySelection, Confirmation {

    private val vm: WishEditViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wish_edit)

        val priorities = resources.getStringArray(R.array.priority)

        if (vm.activeData.value == null) {
            vm.initActiveData(intent.getParcelableExtra("data"))
        }

        vm.activeData.observe(this, Observer {
            if (edit_name.text.isEmpty()) edit_name.setText(it.name)
            if (edit_comment.text.isEmpty()) edit_comment.setText(it.comment)
            val enum = Gson().fromJson(it.category, Categories::class.java)
            if (enum != null) {
                edit_category.text = getText(enum.value)
            } else {
                edit_category.text = it.category
            }
            it.priority?.let { priority ->
                if (priority in 1..5) {
                    edit_priority_title.text = getString(R.string.subtitle_priority, priorities[priority - 1])
                    if (edit_priority.rating.toInt() != priority) edit_priority.rating = priority.toFloat()
                }
            }
        })

        vm.editedData.observe(this, Observer {
            finishWithResultAndData(RESULT_WISH_EDITED, it)
        })

        vm.deletedData.observe(this, Observer {
            finishWithResultAndData(RESULT_WISH_DELETED, it)
        })

        edit_category.setOnClickListener {
            CategoryChooseDialog().apply {
                arguments = Bundle().apply { putString("data", vm.activeData.value?.category) }
                show(supportFragmentManager, "category_dialog")
            }
        }

        edit_save.setOnClickListener {
            vm.updateData(edit_name.text.toString(), edit_comment.text.toString())
        }

        edit_delete.setOnClickListener {
            ConfirmationDialog().also {
                it.arguments = Bundle().apply { putString("message", getString(R.string.message_wish_delete)) }
            }.show(supportFragmentManager, "wish_delete_confirm")
        }

        edit_back.setOnClickListener { finish() }

        edit_priority.setOnRatingBarChangeListener { _, value, _ ->
            if (value < 1) edit_priority.rating = 1f
            else vm.updatePriority(value.toInt())
        }

        vm.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, getString(it), Toast.LENGTH_SHORT).show()
                vm.clearErrorMessage()
            }
        })
    }

    override fun onCategorySelected(category: String?) {
        vm.updateCategory(category)
    }

    override fun onConfirm() {
        vm.deleteData(intent.getParcelableExtra("data"))
    }
}
