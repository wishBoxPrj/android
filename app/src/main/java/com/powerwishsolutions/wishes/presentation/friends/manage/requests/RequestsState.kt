package com.powerwishsolutions.wishes.presentation.friends.manage.requests

import com.powerwishsolutions.wishes.domain.user.model.Profile

sealed class RequestsState {
    object Initial : RequestsState()
    class Loading(val refresh: Boolean) : RequestsState()
    object Error : RequestsState()
    class Success(val model: ArrayList<Profile>) : RequestsState()
}
