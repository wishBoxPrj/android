package com.powerwishsolutions.wishes.presentation.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.data.PreferencesRepository
import com.powerwishsolutions.wishes.presentation.auth.AuthActivity
import org.koin.android.ext.android.get

class LaunchActivity : AppCompatActivity() {

    private val preferencesRepository: PreferencesRepository = get()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        AppCenter.start(application, getString(R.string.analytics_secret), Analytics::class.java, Crashes::class.java)

        Handler().post {
            if (preferencesRepository.getToken() != "") {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                startActivity(Intent(this, AuthActivity::class.java))
            }
            overridePendingTransition(0, 0)
            this.finish()
        }
    }

    override fun onPause() {
        finish()
        super.onPause()
    }
}
