package com.powerwishsolutions.wishes.presentation.plan

import com.powerwishsolutions.wishes.domain.wishlist.model.Wish

sealed class PlanState {
    object Initial : PlanState()
    class Loading(val refresh: Boolean) : PlanState()
    object Error : PlanState()
    class Success(val model: ArrayList<Wish>) : PlanState()
}
