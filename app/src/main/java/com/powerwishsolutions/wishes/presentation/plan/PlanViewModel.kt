package com.powerwishsolutions.wishes.presentation.plan

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.domain.wishlist.repository.WishRepository
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class PlanViewModel(private val repository: WishRepository) : ViewModel() {

    val state = MutableLiveData<PlanState>()
    val recentlyBookedGifts = MutableLiveData<ArrayList<Wish>>()
    val removedGift = MutableLiveData<Wish?>()
    val errorMessage = MutableLiveData<Int>()

    init {
        state.value = PlanState.Initial
        recentlyBookedGifts.value = ArrayList()
        EventBus.getDefault().register(this)
    }

    fun getPlannedGifts(refresh: Boolean) {
        state.postValue(PlanState.Loading(refresh))
        repository.getBookedWishes({
            state.postValue(PlanState.Success(it))
        }, {
            state.postValue(PlanState.Error)
        })
    }

    fun removePlannedGift(gift: Wish) {
        repository.unbookWish(gift, {
            removedGift.postValue(gift)
        }, {
            errorMessage.postValue(R.string.error_cancel_plan)
        })
    }

    fun clearErrorMessage() {
        errorMessage.value = null
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    fun onBookEvent(data: Wish) {
        recentlyBookedGifts.value?.add(data)
        recentlyBookedGifts.postValue(recentlyBookedGifts.value)
    }

    override fun onCleared() {
        EventBus.getDefault().unregister(this)
        super.onCleared()
    }
}
