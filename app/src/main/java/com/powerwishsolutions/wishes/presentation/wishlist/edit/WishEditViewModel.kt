package com.powerwishsolutions.wishes.presentation.wishlist.edit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.domain.wishlist.repository.WishRepository

class WishEditViewModel(private val repository: WishRepository) : ViewModel() {

    val activeData = MutableLiveData<Wish>()
    val editedData = MutableLiveData<Wish>()
    val deletedData = MutableLiveData<Wish>()
    val errorMessage = MutableLiveData<Int>()

    fun initActiveData(wish: Wish) {
        activeData.postValue(wish)
    }

    fun updateData(title: String, comment: String) {
        var commentForPost: String? = null
        if (comment.isNotEmpty()) commentForPost = comment
        val wish = activeData.value
        wish?.name = title
        wish?.comment = commentForPost
        wish?.category = activeData.value?.category
        repository.editWish(wish, {
            editedData.postValue(wish)
        }, {
            errorMessage.postValue(R.string.error_savind_wish)
        })
    }

    fun deleteData(wish: Wish) {
        repository.deleteWish(wish, {
            deletedData.postValue(it)
        }, {
            errorMessage.postValue(R.string.error_deleting_wish)
        })
    }

    fun updateCategory(category: String?) {
        activeData.value?.let {
            it.category = category
            activeData.postValue(it)
        }
    }

    fun updatePriority(priority: Int) {
        activeData.value?.let {
            it.priority = priority
            activeData.postValue(it)
        }
    }

    fun clearErrorMessage() {
        errorMessage.value = null
    }
}
