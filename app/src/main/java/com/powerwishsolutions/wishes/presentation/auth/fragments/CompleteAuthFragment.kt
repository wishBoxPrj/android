package com.powerwishsolutions.wishes.presentation.auth.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.presentation.auth.AuthViewModel
import com.powerwishsolutions.wishes.presentation.common.dialog.date.DatePickerDialog
import com.powerwishsolutions.wishes.utils.callback.DateSelection
import kotlinx.android.synthetic.main.fragment_complete_auth.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.utils.common.DateUtil
import java.util.*

class CompleteAuthFragment : Fragment(), DateSelection {

    private val vm: AuthViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_complete_auth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth_complete.setOnClickListener {
            val date = vm.birthDate.value
            if (auth_user_name.text.isNotEmpty() && date != null) {
                vm.fillProfileData(Profile(auth_user_name.text.toString(), DateUtil.getDatabaseFormat(date), null))
            } else {
                Toast.makeText(context, getString(R.string.error_required_fields), Toast.LENGTH_SHORT).show()
            }
        }

        auth_birth_date.setOnClickListener {
            fragmentManager?.let {
                val dialog = DatePickerDialog()
                dialog.setTargetFragment(this, 0)
                dialog.show(it, "date-picker")
            }
        }

        vm.birthDate.observe(this, Observer {
            val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
            auth_birth_date.text = dateFormat.format(it.time)
        })
    }

    override fun onDateSelected(calendar: Calendar) {
        vm.setBirthDate(calendar)
    }
}
