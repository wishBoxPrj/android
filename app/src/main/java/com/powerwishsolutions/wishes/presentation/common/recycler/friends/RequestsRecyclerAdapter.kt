package com.powerwishsolutions.wishes.presentation.common.recycler.friends

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.extensions.setAvatar
import com.powerwishsolutions.wishes.utils.common.ACTION_ACCEPT_REQUEST
import com.powerwishsolutions.wishes.utils.common.ACTION_DENY_REQUEST
import kotlinx.android.synthetic.main.item_request.view.*

class RequestsRecyclerAdapter(
    private val list: ArrayList<Profile>,
    private val context: Context?,
    val listener: (action: Int, position: Int) -> Unit,
    val emptyListener: (empty: Boolean) -> Unit
) : RecyclerView.Adapter<RequestsRecyclerAdapter.RequestsViewHolder>() {

    init { emptyListener(list.size == 0) }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): RequestsViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val itemView = inflater.inflate(R.layout.item_request, viewGroup, false)
        return RequestsViewHolder(
            itemView
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RequestsViewHolder, position: Int) {
        holder.name.text = context?.getString(R.string.friend_name, list[position].owner?.first_name, list[position].owner?.last_name)
        holder.nickname.text = context?.getString(R.string.placeholder_username, list[position].nickname)

        holder.accept.setOnClickListener { listener(ACTION_ACCEPT_REQUEST, position) }
        holder.deny.setOnClickListener { listener(ACTION_DENY_REQUEST, position) }

        holder.avatar.setAvatar(context, holder.letter, list[position])
    }

    fun remove(friend: Profile) {
        if (list.contains(friend)) {
            val pos = list.indexOf(friend)
            list.remove(friend)
            this.notifyItemRemoved(pos)
            this.notifyItemRangeChanged(pos, list.size)
            emptyListener(list.size == 0)
        }
    }

    class RequestsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.item_request_name
        val nickname: TextView = itemView.item_request_nickname
        val letter: TextView = itemView.item_request_letter
        val avatar: ImageView = itemView.item_request_avatar
        val accept: Button = itemView.item_request_accept
        val deny: Button = itemView.item_request_deny
    }
}
