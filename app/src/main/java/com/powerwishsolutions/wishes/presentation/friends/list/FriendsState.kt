package com.powerwishsolutions.wishes.presentation.friends.list

import com.powerwishsolutions.wishes.domain.user.model.Profile

sealed class FriendsState {
    object Initial : FriendsState()
    class Loading(val refresh: Boolean) : FriendsState()
    class Success(val model: ArrayList<Profile>) : FriendsState()
    object Error : FriendsState()
}
