package com.powerwishsolutions.wishes.presentation.wishlist.create

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Categories
import com.powerwishsolutions.wishes.extensions.finishWithResultAndData
import com.powerwishsolutions.wishes.presentation.common.dialog.category.CategoryChooseDialog
import com.powerwishsolutions.wishes.utils.callback.CategorySelection
import com.powerwishsolutions.wishes.utils.common.RESULT_WISH_CREATED
import kotlinx.android.synthetic.main.activity_wish_create.*
import org.koin.android.viewmodel.ext.android.viewModel

class WishCreateActivity : AppCompatActivity(), CategorySelection {

    val vm: WishCreateViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wish_create)

        val priorities = resources.getStringArray(R.array.priority)
        create_back.setOnClickListener { finish() }

        create_save.setOnClickListener {
            val title = create_name.text.toString()
            val comment = create_comment.text.toString()
            if (title.isNotEmpty()) vm.createWish(title, comment)
        }

        create_category.setOnClickListener {
            CategoryChooseDialog().apply {
                arguments = Bundle().apply { putString("data", vm.category.value) }
                show(supportFragmentManager, "category_dialog")
            }
        }

        vm.category.observe(this, Observer {
            val enum = Gson().fromJson(it, Categories::class.java)
            if (enum != null) {
                create_category.text = getText(enum.value)
            } else {
                create_category.text = it
            }
        })

        vm.createdData.observe(this, Observer {
            finishWithResultAndData(RESULT_WISH_CREATED, it)
        })

        vm.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, getString(it), Toast.LENGTH_SHORT).show()
                vm.clearErrorMessage()
            }
        })

        create_priority.setOnRatingBarChangeListener { _, value, _ ->
            if (value < 1) create_priority.rating = 1f
            else vm.setPriority(value.toInt())
        }

        vm.priority.observe(this, Observer {
            create_priority_title.text = getString(R.string.subtitle_priority, priorities[it - 1])
        })
    }

    override fun onCategorySelected(category: String?) {
        vm.setCategory(category)
    }
}
