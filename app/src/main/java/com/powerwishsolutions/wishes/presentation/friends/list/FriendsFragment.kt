package com.powerwishsolutions.wishes.presentation.friends.list

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.extensions.applyErrorState
import com.powerwishsolutions.wishes.extensions.applyLoadingState
import com.powerwishsolutions.wishes.extensions.applySuccessState
import com.powerwishsolutions.wishes.extensions.setLinearOffsets
import com.powerwishsolutions.wishes.presentation.common.recycler.friends.FriendsRecyclerAdapter
import com.powerwishsolutions.wishes.presentation.friends.manage.ManageFriendsActivity
import com.powerwishsolutions.wishes.presentation.friends.wishlist.FriendWishListActivity
import com.powerwishsolutions.wishes.utils.common.RESULT_FRIENDS_UPDATED
import kotlinx.android.synthetic.main.fragment_friends.*
import org.koin.android.viewmodel.ext.android.viewModel

class FriendsFragment : Fragment() {

    private val vm: FriendsViewModel by viewModel()
    private lateinit var adapter: FriendsRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_friends, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        friends_recycler.setLinearOffsets(
            context,
            R.dimen.item_offset_0,
            R.dimen.item_offset_0,
            R.dimen.item_offset_12,
            R.dimen.item_offset_12
        )

        vm.state.observe(this, Observer {
            when (it) {
                is FriendsState.Initial -> { vm.getFriends(false) }
                is FriendsState.Loading -> {
                    applyLoadingState(it.refresh, friends_recycler, friends_loader, friends_error, friends_refresh)
                }
                is FriendsState.Success -> {
                    applySuccessState(friends_loader, friends_error, friends_refresh)
                    friends_recycler.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                    adapter = FriendsRecyclerAdapter(it.model, context, { position ->
                        startActivity(Intent(context, FriendWishListActivity::class.java).apply {
                            putExtra("data", it.model[position])
                        })
                    }, { id, position ->
                        if (id == R.id.friend_remove && position < it.model.size) vm.deleteFriend(it.model[position])
                    }, { empty -> friends_empty.isVisible = empty })

                    friends_recycler.adapter = adapter
                    vm.deletedFriend.observe(this, Observer { friend ->
                        if (friend != null) {
                            adapter.remove(friend)
                            vm.deletedFriend.value = null
                        }
                    })
                }
                is FriendsState.Error -> {
                    friends_empty.isVisible = false
                    applyErrorState(friends_recycler, friends_loader, friends_error, friends_refresh)
                    friends_error.setActionListener { vm.getFriends(false) }
                    vm.deletedFriend.removeObservers(this)
                }
            }
        })

        friends_refresh.setOnRefreshListener { vm.getFriends(true) }
        friends_add.setOnClickListener { startActivityForResult(Intent(context, ManageFriendsActivity::class.java), RESULT_FRIENDS_UPDATED) }

        vm.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(context, getString(it), Toast.LENGTH_SHORT).show()
                vm.clearErrorMessage()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_FRIENDS_UPDATED) {
            vm.getFriends(false)
        } else super.onActivityResult(requestCode, resultCode, data)
    }
}
