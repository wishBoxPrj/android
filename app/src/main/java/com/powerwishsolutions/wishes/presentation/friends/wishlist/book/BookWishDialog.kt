package com.powerwishsolutions.wishes.presentation.friends.wishlist.book

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.presentation.friends.wishlist.FriendWishListViewModel
import kotlinx.android.synthetic.main.fragment_book_wish_dialog.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class BookWishDialog : DialogFragment() {

    private val vm: FriendWishListViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_book_wish_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val wish = arguments?.getParcelable("data") as Wish?
        book_wish_name.text = wish?.name
        book_wish_comment.text = wish?.comment
        book_wish_comment.isVisible = !wish?.comment.isNullOrEmpty()

        book_wish_cancel.setOnClickListener { dismiss() }
        book_wish_apply.setOnClickListener {
            vm.bookWish(wish)
            dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setContentView(R.layout.fragment_book_wish_dialog)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            window?.attributes?.windowAnimations = R.style.DialogSlideAnimation
            window?.attributes?.gravity = Gravity.BOTTOM
            window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }
}
