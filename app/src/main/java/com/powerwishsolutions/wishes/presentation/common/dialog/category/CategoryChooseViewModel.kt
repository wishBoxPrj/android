package com.powerwishsolutions.wishes.presentation.common.dialog.category

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.domain.wishlist.repository.WishRepository

class CategoryChooseViewModel(private val repository: WishRepository) : ViewModel() {

    val state = MutableLiveData<CategoryChooseState>()
    var selected: String? = null

    init {
        state.value = CategoryChooseState.Initial
    }

    fun getCategories() {
        state.postValue(CategoryChooseState.Loading)
        repository.getCategories({
            state.postValue(CategoryChooseState.Success(it))
        }, { })
    }
}
