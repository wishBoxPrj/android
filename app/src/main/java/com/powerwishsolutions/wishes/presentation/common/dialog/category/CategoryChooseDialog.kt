package com.powerwishsolutions.wishes.presentation.common.dialog.category

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.presentation.common.recycler.wishes.CategoryRecyclerAdapter
import com.powerwishsolutions.wishes.utils.callback.CategorySelection
import kotlinx.android.synthetic.main.fragment_category_choose_dialog.*
import org.koin.android.viewmodel.ext.android.viewModel

class CategoryChooseDialog : DialogFragment() {

    private val vm: CategoryChooseViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_category_choose_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (vm.state.value is CategoryChooseState.Initial) {
            val selected = arguments?.getString("data")
            vm.selected = selected
            vm.getCategories()
        }

        vm.state.observe(this, Observer {
            when (it) {
                is CategoryChooseState.Loading -> {
                    choose_category_loader.visibility = View.VISIBLE
                }
                is CategoryChooseState.Success -> {
                    choose_category_loader.visibility = View.GONE
                    choose_category_recycler.visibility = View.VISIBLE
                    choose_category_apply.visibility = View.VISIBLE
                    choose_category_recycler.adapter = CategoryRecyclerAdapter(context, it.model, vm.selected) { category ->
                        vm.selected = category
                    }
                    choose_category_apply.setOnClickListener {
                        val callback = activity as CategorySelection?
                        callback?.onCategorySelected(vm.selected)
                        dismiss()
                    }
                }
            }
        })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setContentView(R.layout.fragment_category_choose_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }
}
