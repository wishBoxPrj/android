package com.powerwishsolutions.wishes.presentation.common.pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.powerwishsolutions.wishes.presentation.auth.fragments.AuthFragment
import com.powerwishsolutions.wishes.presentation.auth.fragments.CompleteAuthFragment

class AuthFragmentAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val authFragment = AuthFragment()
    private val completeAuthFragment = CompleteAuthFragment()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> authFragment
            1 -> completeAuthFragment
            else -> authFragment
        }
    }

    override fun getCount(): Int {
        return 2
    }
}
