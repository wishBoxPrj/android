package com.powerwishsolutions.wishes.presentation.friends.manage.requests

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.extensions.applyErrorState
import com.powerwishsolutions.wishes.extensions.applyLoadingState
import com.powerwishsolutions.wishes.extensions.applySuccessState
import com.powerwishsolutions.wishes.extensions.setLinearOffsets
import com.powerwishsolutions.wishes.presentation.common.recycler.friends.RequestsRecyclerAdapter
import com.powerwishsolutions.wishes.presentation.friends.manage.ManageFriendsViewModel
import com.powerwishsolutions.wishes.utils.common.ACTION_ACCEPT_REQUEST
import com.powerwishsolutions.wishes.utils.common.ACTION_DENY_REQUEST
import kotlinx.android.synthetic.main.fragment_friend_requests.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class FriendRequestsFragment : Fragment() {

    private val vm: ManageFriendsViewModel by sharedViewModel()
    private lateinit var adapter: RequestsRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_friend_requests, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requests_recycler.setLinearOffsets(
            context,
            R.dimen.item_offset_0,
            R.dimen.item_offset_0,
            R.dimen.item_offset_0,
            R.dimen.item_offset_12
        )

        vm.requestState.observe(this, Observer {
            when (it) {
                is RequestsState.Initial -> { vm.getIncomingRequests(false) }
                is RequestsState.Loading -> {
                    applyLoadingState(it.refresh, requests_recycler, requests_loader, requests_error)
                }
                is RequestsState.Success -> {
                    applySuccessState(requests_loader, requests_error, requests_refresh)

                    requests_recycler.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                    adapter = RequestsRecyclerAdapter(it.model, context, { action, position ->
                        when (action) {
                            ACTION_ACCEPT_REQUEST -> vm.acceptRequest(it.model[position])
                            ACTION_DENY_REQUEST -> vm.denyRequest(it.model[position])
                        }
                    }, { empty -> requests_empty.isVisible = empty })
                    requests_recycler.adapter = adapter

                    vm.processedRequest.observe(this, Observer { request ->
                        if (request != null) {
                            adapter.remove(request)
                            vm.processedRequest.value = null
                        }
                    })
                }
                is RequestsState.Error -> {
                    requests_empty.isVisible = false
                    applyErrorState(requests_recycler, requests_loader, requests_error, requests_refresh)
                    requests_error.setActionListener { vm.getIncomingRequests(false) }
                    vm.processedRequest.removeObservers(this)
                }
            }
        })

        requests_refresh.setOnRefreshListener {
            vm.getIncomingRequests(true)
        }
    }
}
