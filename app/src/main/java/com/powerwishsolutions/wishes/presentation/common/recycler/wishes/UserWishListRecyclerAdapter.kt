package com.powerwishsolutions.wishes.presentation.common.recycler.wishes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.presentation.common.view.SvgRatingBar
import kotlinx.android.synthetic.main.item_wish_list.view.*

class UserWishListRecyclerAdapter(
    private val list: ArrayList<Wish>,
    val listener: (Int) -> Unit,
    val emptyListener: (empty: Boolean) -> Unit
) : RecyclerView.Adapter<UserWishListRecyclerAdapter.UserWishListViewHolder>() {

    init { emptyListener(list.size == 0) }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): UserWishListViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val itemView = inflater.inflate(R.layout.item_wish_list, viewGroup, false)
        return UserWishListViewHolder(
            itemView
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: UserWishListViewHolder, position: Int) {
        holder.name.text = list[position].name
        holder.comment.text = list[position].comment
        holder.comment.isVisible = !list[position].comment.isNullOrEmpty()
        list[position].priority?.toFloat()?.let { holder.priority.rating = it }
        holder.itemView.setOnClickListener { listener(position) }
    }

    class UserWishListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.item_wish_list_name
        val comment: TextView = itemView.item_wish_list_comment
        val priority: SvgRatingBar = itemView.item_wish_list_priority
    }

    fun add(wish: Wish) {
        list.add(0, wish)
        notifyItemInserted(0)
        notifyItemRangeChanged(1, list.size)
        emptyListener(list.size == 0)
    }

    fun edit(wish: Wish) {
        val value = list.find { it.id == wish.id }
        if (value != null) {
            val i = list.indexOf(value)
            list[i] = wish
            notifyItemChanged(i)
        }
    }

    fun remove(wish: Wish) {
        val i = list.indexOf(wish)
        list.remove(wish)
        notifyItemRemoved(i)
        notifyItemRangeChanged(i, list.size)
        emptyListener(list.size == 0)
    }
}
