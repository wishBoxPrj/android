package com.powerwishsolutions.wishes.presentation.common.recycler.friends

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.extensions.setAvatar
import kotlinx.android.synthetic.main.item_friend_search.view.*

class SearchFriendsRecyclerAdapter(
    private val list: ArrayList<Profile>,
    private val requestedList: ArrayList<Profile>,
    private val context: Context?,
    val listener: (Int) -> Unit
) : RecyclerView.Adapter<SearchFriendsRecyclerAdapter.SearchFriendsViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): SearchFriendsViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val itemView = inflater.inflate(R.layout.item_friend_search, viewGroup, false)
        return SearchFriendsViewHolder(
            itemView
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SearchFriendsViewHolder, position: Int) {
        holder.name.text = context?.getString(R.string.friend_name, list[position].owner?.first_name, list[position].owner?.last_name)
        holder.username.text = context?.getString(R.string.placeholder_username, list[position].nickname)

        if (requestedList.contains(list[position])) {
            holder.add.setImageResource(R.drawable.ic_dropdown_done)
            holder.add.setOnClickListener(null)
        } else {
            holder.add.setImageResource(R.drawable.ic_send_request)
            holder.add.setOnClickListener {
                listener(position)
            }
        }

        holder.avatar.setAvatar(context, holder.letter, list[position])
    }

    fun updateRequested() {
        for (friend in requestedList) {
            if (list.contains(friend)) {
                val pos = list.indexOf(friend)
                this.notifyItemChanged(pos)
            }
        }
    }

    class SearchFriendsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.item_friend_search_name
        val username: TextView = itemView.item_friend_search_username
        val letter: TextView = itemView.item_friend_search_letter
        val avatar: ImageView = itemView.item_friend_search_avatar
        val add: ImageView = itemView.item_friend_search_add
    }
}
