package com.powerwishsolutions.wishes.presentation.common.recycler.friends

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.extensions.setAvatar
import com.powerwishsolutions.wishes.utils.common.DateUtil
import com.shehabic.droppy.DroppyMenuPopup
import com.shehabic.droppy.animations.DroppyFadeInAnimation
import kotlinx.android.synthetic.main.item_friend.view.*

class FriendsRecyclerAdapter(
    private val list: ArrayList<Profile>,
    private val context: Context?,
    val listener: (Int) -> Unit,
    val longTapListener: (id: Int, position: Int) -> Unit,
    val emptyListener: (empty: Boolean) -> Unit
) : RecyclerView.Adapter<FriendsRecyclerAdapter.FriendsViewHolder>() {

    init { emptyListener(list.size == 0) }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): FriendsViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val itemView = inflater.inflate(R.layout.item_friend, viewGroup, false)
        return FriendsViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: FriendsViewHolder, position: Int) {
        holder.name.text = context?.getString(R.string.friend_name, list[position].owner?.first_name, list[position].owner?.last_name)
        holder.dateOfBirth.text = DateUtil.getAppDateFormat(list[position].date_of_birth)
        holder.itemView.setOnClickListener { listener(position) }

        val dropDownBuilder = DroppyMenuPopup.Builder(context, holder.anchor)
        val dropDown = dropDownBuilder.fromMenu(R.menu.friend_dropdown)
            .triggerOnAnchorClick(false)
            .setOnClick { _, id ->
                longTapListener(id, position)
            }
            .setPopupAnimation(DroppyFadeInAnimation())
            .build()

        holder.itemView.setOnLongClickListener {
            dropDown.show()
            true
        }

        holder.avatar.setAvatar(context, holder.letter, list[position])
    }

    fun remove(friend: Profile) {
        val i = list.indexOf(friend)
        list.remove(friend)
        notifyItemRemoved(i)
        notifyItemRangeChanged(i, list.size)
        emptyListener(list.size == 0)
    }

    class FriendsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.item_friend_name
        val dateOfBirth: TextView = itemView.item_friend_birth
        val letter: TextView = itemView.item_friend_letter
        val avatar: ImageView = itemView.item_friend_avatar
        val anchor: View = itemView.item_friend_anchor
    }
}
