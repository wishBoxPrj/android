package com.powerwishsolutions.wishes.presentation.common.recycler.wishes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Categories
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryRecyclerAdapter(
    private val context: Context?,
    private val list: ArrayList<String?>,
    private var selected: String?,
    val listener: (String?) -> Unit
) : RecyclerView.Adapter<CategoryRecyclerAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): CategoryViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val itemView = inflater.inflate(R.layout.item_category, viewGroup, false)
        return CategoryViewHolder(
            itemView
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {

        val category = Gson().fromJson(list[position], Categories::class.java)

        if (category != null) {
            holder.check.text = context?.getText(category.value)
        } else {
            holder.check.text = list[position]
        }

        holder.check.isChecked = list[position] == selected

        holder.check.setOnClickListener {
            listener(list[position])
            changeSelection(list[position])
        }
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val check: RadioButton = itemView.item_category_check
    }

    private fun changeSelection(category: String?) {
        if (selected != null) {
            val previousPosition = list.indexOf(selected)
            if (previousPosition in 0 until list.size) {
                selected = category
                notifyItemChanged(previousPosition)
            }
        }
    }
}
