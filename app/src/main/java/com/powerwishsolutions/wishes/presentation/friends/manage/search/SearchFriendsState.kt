package com.powerwishsolutions.wishes.presentation.friends.manage.search

import com.powerwishsolutions.wishes.domain.user.model.Profile

sealed class SearchFriendsState {
    object Initial : SearchFriendsState()
    object Loading : SearchFriendsState()
    object Error : SearchFriendsState()
    class Success(val model: ArrayList<Profile>) : SearchFriendsState()
}
