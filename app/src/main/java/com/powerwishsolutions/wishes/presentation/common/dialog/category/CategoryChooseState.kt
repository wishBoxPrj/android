package com.powerwishsolutions.wishes.presentation.common.dialog.category

sealed class CategoryChooseState {
    object Initial : CategoryChooseState()
    object Loading : CategoryChooseState()
    class Success(val model: ArrayList<String?>) : CategoryChooseState()
}
