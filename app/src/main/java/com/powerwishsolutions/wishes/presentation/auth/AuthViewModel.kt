package com.powerwishsolutions.wishes.presentation.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.AuthForm
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.user.model.User
import com.powerwishsolutions.wishes.domain.user.repository.UserRepository
import java.util.*

class AuthViewModel(private val repository: UserRepository) : ViewModel() {

    val state = MutableLiveData<AuthState>()
    val birthDate = MutableLiveData<Calendar>()
    val error = MutableLiveData<Int>()

    private var user: User? = null

    init {
        state.value = AuthState.Initial
    }

    fun authWithVk(code: String) {
        state.postValue(AuthState.Authorizing)
        repository.auth(AuthForm(code), { user ->
            repository.getProfile("Token ${user.token}", {
                this.user = user
                if (it.nickname.isNullOrEmpty()) { state.postValue(AuthState.NeedAddictionData) } else { completeAuth(it) }
            }, { throwAuthError() })
        }, { throwAuthError() })
    }

    fun fillProfileData(profile: Profile) {
        if (user != null) {
            repository.setProfile("Token ${user?.token}", profile, {
                completeAuth(it)
            }, {
                error.postValue(R.string.error_auth_complete)
            })
        }
    }

    fun setBirthDate(date: Calendar) {
        birthDate.postValue(date)
    }

    private fun throwAuthError() {
        state.postValue(AuthState.Initial)
        error.postValue(R.string.error_auth_vk)
    }

    private fun completeAuth(profile: Profile?) {
        repository.saveUser(user, profile)
        state.postValue(AuthState.Authorized)
    }
}
