package com.powerwishsolutions.wishes.presentation.friends.list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.friends.repository.FriendsRepository
import com.powerwishsolutions.wishes.domain.user.model.Profile
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class FriendsViewModel(private val repository: FriendsRepository) : ViewModel() {

    val state = MutableLiveData<FriendsState>()
    val errorMessage = MutableLiveData<Int>()
    val deletedFriend = MutableLiveData<Profile>()

    init {
        state.value = FriendsState.Initial
        EventBus.getDefault().register(this)
    }

    fun getFriends(refresh: Boolean) {
        state.postValue(FriendsState.Loading(refresh))
        repository.getFriends({
            state.postValue(FriendsState.Success(it))
        }, {
            state.postValue(FriendsState.Error)
        })
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    fun onFriendDelete(data: Profile) {
        Log.e("FRIENDS", "DELETED ${data.nickname}")
        deletedFriend.postValue(data)
    }

    fun deleteFriend(friend: Profile) {
        repository.deleteFriend(friend, {
            deletedFriend.postValue(it)
        }, {
            errorMessage.postValue(R.string.error_friend_delete)
        })
    }

    fun clearErrorMessage() {
        errorMessage.value = null
    }

    override fun onCleared() {
        EventBus.getDefault().unregister(this)
        super.onCleared()
    }
}
