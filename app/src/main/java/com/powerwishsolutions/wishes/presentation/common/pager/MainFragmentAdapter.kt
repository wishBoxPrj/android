package com.powerwishsolutions.wishes.presentation.common.pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.powerwishsolutions.wishes.presentation.friends.list.FriendsFragment
import com.powerwishsolutions.wishes.presentation.plan.PlanFragment
import com.powerwishsolutions.wishes.presentation.wishlist.list.WishListFragment

class MainFragmentAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val friendsFragment = FriendsFragment()
    private val planFragment = PlanFragment()
    private val wishListFragment = WishListFragment()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> friendsFragment
            1 -> planFragment
            2 -> wishListFragment
            else -> friendsFragment
        }
    }

    override fun getCount(): Int {
        return 3
    }
}
