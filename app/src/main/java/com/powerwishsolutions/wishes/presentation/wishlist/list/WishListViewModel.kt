package com.powerwishsolutions.wishes.presentation.wishlist.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.user.repository.UserRepository
import com.powerwishsolutions.wishes.domain.wishlist.repository.WishRepository

class WishListViewModel(
    private val wishRepository: WishRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    val state = MutableLiveData<WishListState>()
    val profileInfo = MutableLiveData<Profile>()

    init {
        state.value = WishListState.Initial
    }

    fun getWishes(refresh: Boolean) {
        state.postValue(WishListState.Loading(refresh))
        wishRepository.getWishes({
            state.postValue(WishListState.Success(it))
        }, {
            state.postValue(WishListState.Error)
        })
    }

    fun getProfileInfo() {
        userRepository.getProfile({
            profileInfo.postValue(it)
        }, { })
    }
}
