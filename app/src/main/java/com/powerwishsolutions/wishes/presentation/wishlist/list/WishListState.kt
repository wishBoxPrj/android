package com.powerwishsolutions.wishes.presentation.wishlist.list

import com.powerwishsolutions.wishes.domain.wishlist.model.Wish

sealed class WishListState {
    object Initial : WishListState()
    object Error : WishListState()
    class Loading(val refresh: Boolean) : WishListState()
    class Success(val model: ArrayList<Wish>) : WishListState()
}
