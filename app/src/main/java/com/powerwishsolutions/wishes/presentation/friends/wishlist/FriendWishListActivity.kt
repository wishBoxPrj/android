package com.powerwishsolutions.wishes.presentation.friends.wishlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.extensions.applyErrorState
import com.powerwishsolutions.wishes.extensions.applyLoadingState
import com.powerwishsolutions.wishes.extensions.applySuccessState
import com.powerwishsolutions.wishes.extensions.setLinearOffsets
import com.powerwishsolutions.wishes.presentation.common.dialog.confirmation.ConfirmationDialog
import com.powerwishsolutions.wishes.presentation.common.recycler.wishes.FriendWishesRecyclerAdapter
import com.powerwishsolutions.wishes.presentation.friends.wishlist.book.BookWishDialog
import com.powerwishsolutions.wishes.utils.callback.Confirmation
import kotlinx.android.synthetic.main.activity_friend_wish_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class FriendWishListActivity : AppCompatActivity(), Confirmation {

    private val vm: FriendWishListViewModel by viewModel()
    private lateinit var adapter: FriendWishesRecyclerAdapter
    var profile: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friend_wish_list)

        profile = intent.getParcelableExtra("data") as Profile?
        friend_wish_list_title.text = getString(R.string.placeholder_username, profile?.nickname)

        friend_wish_list_recycler.setLinearOffsets(
            this,
            R.dimen.item_offset_14,
            R.dimen.item_offset_20,
            R.dimen.item_offset_0,
            R.dimen.item_offset_14
        )

        vm.state.observe(this, Observer {
            when (it) {
                is FriendWishListState.Initial -> { vm.loadWishList(profile?.nickname) }
                is FriendWishListState.Loading -> {
                    applyLoadingState(friend_wish_list_recycler, friend_wish_list_loader, friend_wish_list_error)
                }
                is FriendWishListState.Success -> {
                    applySuccessState(friend_wish_list_loader, friend_wish_list_error)

                    friend_wish_list_recycler.layoutAnimation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_fall_down)
                    adapter = FriendWishesRecyclerAdapter(this, profile, it.model, { position ->
                        BookWishDialog().apply {
                            arguments = Bundle().apply { putParcelable("data", it.model[position]) }
                            show(supportFragmentManager, "book_wish")
                        }
                    }, { empty -> friend_wish_list_empty.isVisible = empty })
                    friend_wish_list_recycler.adapter = adapter

                    vm.bookedWish.observe(this, Observer { wish ->
                        if (wish != null) {
                            adapter.markAsBooked(wish)
                            vm.bookedWish.value = null
                        }
                    })
                }
                is FriendWishListState.Error -> {
                    friend_wish_list_empty.isVisible = false
                    applyErrorState(friend_wish_list_recycler, friend_wish_list_loader, friend_wish_list_error)
                    friend_wish_list_error.setActionListener { vm.loadWishList(profile?.nickname) }
                }
            }
        })

        friend_wish_list_delete.setOnClickListener {
            ConfirmationDialog().also {
                it.arguments = Bundle().apply {
                    putString("message", getString(R.string.message_friend_delete, profile?.nickname))
                }
            }.show(supportFragmentManager, "confirmation")
        }
        friend_wish_list_back.setOnClickListener { finish() }
        vm.friendDeleted.observe(this, Observer { if (it) finish() })

        vm.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, getString(it), Toast.LENGTH_SHORT).show()
                vm.clearErrorMessage()
            }
        })
    }

    override fun onConfirm() {
        profile?.let { vm.deleteFriend(it) }
    }
}
