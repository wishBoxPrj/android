package com.powerwishsolutions.wishes.presentation.auth

sealed class AuthState {
    object Initial : AuthState()
    object Authorizing : AuthState()
    object NeedAddictionData : AuthState()
    object Authorized : AuthState()
}
