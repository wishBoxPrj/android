package com.powerwishsolutions.wishes.presentation.auth.web

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.*
import com.powerwishsolutions.wishes.R
import kotlinx.android.synthetic.main.activity_web_auth.*

class WebAuthActivity : AppCompatActivity() {

    private val vkAuthUrl = "https://oauth.vk.com/authorize?client_id=7148970&display=page&redirect_uri=http://powerwishes.me/&response_type=code"
    private val cookieManager = CookieManager.getInstance()

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_auth)

        cookieManager.removeAllCookies { auth_web_view.loadUrl(vkAuthUrl) }
        auth_web_view.settings.javaScriptEnabled = true
        auth_web_view.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                val uri = Uri.parse(url)
                val code = uri.getQueryParameter("code")
                val error = uri.getQueryParameter("error")
                when {
                    code != null -> {
                        val dataIntent = Intent()
                        dataIntent.putExtra("code", code)
                        setResult(21, dataIntent)
                        finish()
                    }
                    error != null -> {
                        setResult(31)
                        finish()
                    }
                    else -> view?.loadUrl(url)
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                auth_web_refresh.isRefreshing = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                auth_web_refresh.isRefreshing = false
            }
        }

        auth_web_refresh.setOnRefreshListener { auth_web_view.loadUrl(vkAuthUrl) }
        auth_web_back.setOnClickListener { finish() }
    }
}
