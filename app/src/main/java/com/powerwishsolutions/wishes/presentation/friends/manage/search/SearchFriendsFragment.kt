package com.powerwishsolutions.wishes.presentation.friends.manage.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.SearchView
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.extensions.applyErrorState
import com.powerwishsolutions.wishes.extensions.applyLoadingState
import com.powerwishsolutions.wishes.extensions.applySuccessState
import com.powerwishsolutions.wishes.extensions.setLinearOffsets
import com.powerwishsolutions.wishes.presentation.common.recycler.friends.SearchFriendsRecyclerAdapter
import com.powerwishsolutions.wishes.presentation.friends.manage.ManageFriendsViewModel
import kotlinx.android.synthetic.main.fragment_search_friends.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class SearchFriendsFragment : Fragment() {

    private val vm: ManageFriendsViewModel by sharedViewModel()
    private lateinit var adapter: SearchFriendsRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_friends, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        search_friends_rv.setLinearOffsets(
            context,
            R.dimen.item_offset_0,
            R.dimen.item_offset_0,
            R.dimen.item_offset_12,
            R.dimen.item_offset_14
        )

        search_friends_searchbox.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                vm.searchFriends(query)
                search_friends_searchbox.clearFocus()
                return false
            }

            override fun onQueryTextChange(s: String): Boolean { return false }
        })

        vm.searchState.observe(this, Observer {
            when (it) {
                is SearchFriendsState.Loading -> {
                    applyLoadingState(false, search_friends_rv, search_friends_loader, search_friends_error)
                }
                is SearchFriendsState.Success -> {
                    applySuccessState(search_friends_loader, search_friends_error)

                    search_friends_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                    adapter = SearchFriendsRecyclerAdapter(it.model, vm.requestedFriends.value!!, context) { position ->
                        vm.addRequestedFriend(it.model[position])
                    }

                    search_friends_rv.adapter = adapter
                    vm.requestedFriends.observe(this, Observer {
                        adapter.updateRequested()
                    })
                }
                is SearchFriendsState.Error -> {
                    applyErrorState(search_friends_rv, search_friends_loader, search_friends_error)
                    search_friends_error.setActionListener { vm.retrySearch() }
                    vm.requestedFriends.removeObservers(this)
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        search_friends_root.requestFocus()
    }
}
