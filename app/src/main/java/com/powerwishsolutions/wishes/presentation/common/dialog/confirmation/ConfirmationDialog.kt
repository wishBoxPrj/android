package com.powerwishsolutions.wishes.presentation.common.dialog.confirmation

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.utils.callback.Confirmation
import kotlinx.android.synthetic.main.fragment_confirmation_dialog.*

class ConfirmationDialog : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_confirmation_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val message = arguments?.getString("message")
        confirmation_message.text = Html.fromHtml(message)

        confirmation_cancel.setOnClickListener { dismiss() }

        confirmation_ok.setOnClickListener {
            (activity as Confirmation?)?.onConfirm()
            dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setContentView(R.layout.fragment_category_choose_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }
}
