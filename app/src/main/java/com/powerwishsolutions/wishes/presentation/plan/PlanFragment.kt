package com.powerwishsolutions.wishes.presentation.plan

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.extensions.*
import com.powerwishsolutions.wishes.presentation.common.recycler.gifts.PlannedGiftsRecyclerAdapter
import com.powerwishsolutions.wishes.presentation.friends.wishlist.FriendWishListActivity
import kotlinx.android.synthetic.main.fragment_plan.*
import org.koin.android.viewmodel.ext.android.viewModel

class PlanFragment : Fragment() {

    private val vm: PlanViewModel by viewModel()
    private lateinit var adapter: PlannedGiftsRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_plan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        plan_recycler.setLinearOffsets(
            context,
            R.dimen.item_offset_14,
            R.dimen.item_offset_20,
            R.dimen.item_offset_14,
            R.dimen.item_offset_14
        )

        vm.state.observe(this, Observer {
            when (it) {
                is PlanState.Initial -> { vm.getPlannedGifts(false) }
                is PlanState.Loading -> {
                    applyLoadingState(it.refresh, plan_recycler, plan_loader, plan_error, plan_refresh)
                }
                is PlanState.Success -> {
                    applySuccessState(plan_loader, plan_error, plan_refresh)
                    plan_recycler.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                    adapter = PlannedGiftsRecyclerAdapter(context, it.model, {
                        // TODO: open item activity with comment and marketplace offers
                    }, { id, position ->
                        if (position < it.model.size) {
                            when (id) {
                                R.id.gift_cancel -> {
                                    vm.removePlannedGift(it.model[position])
                                }
                                R.id.gift_profile -> {
                                    val intent = Intent(context, FriendWishListActivity::class.java)
                                    intent.putExtra("data", it.model[position].owner)
                                    startActivity(intent)
                                }
                            }
                        }
                    }, { empty -> plan_empty.isVisible = empty })
                    plan_recycler.adapter = adapter

                    vm.recentlyBookedGifts.observe(this, Observer { booked ->
                        if (booked.isNotEmpty()) {
                            for (plannedGift in booked) adapter.add(plannedGift)
                            booked.clear()
                        }
                    })

                    vm.removedGift.observe(this, Observer { gift ->
                        if (gift != null) {
                            adapter.remove(gift)
                            vm.removedGift.value = null
                        }
                    })
                }
                is PlanState.Error -> {
                    plan_empty.isVisible = false
                    applyErrorState(plan_recycler, plan_loader, plan_error, plan_refresh)
                    plan_error.setActionListener { vm.getPlannedGifts(false) }
                }
            }
        })
        plan_refresh.setOnRefreshListener { vm.getPlannedGifts(true) }

        vm.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(context, getString(it), Toast.LENGTH_SHORT).show()
                vm.clearErrorMessage()
            }
        })
    }
}
