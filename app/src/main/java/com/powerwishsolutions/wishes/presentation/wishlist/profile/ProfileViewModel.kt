package com.powerwishsolutions.wishes.presentation.wishlist.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.user.model.User
import com.powerwishsolutions.wishes.domain.user.repository.UserRepository
import com.powerwishsolutions.wishes.utils.common.DateUtil
import java.util.*

class ProfileViewModel(private val repository: UserRepository) : ViewModel() {

    val userInfo = MutableLiveData<User>()
    val userProfile = MutableLiveData<Profile>()
    val errorMessage = MutableLiveData<Int>()

    fun getUserInfo() {
        repository.getUser({ userInfo.postValue(it) }, { })
    }

    fun getUserProfile() {
        repository.getProfile({
            it.date_of_birth = DateUtil.getAppDateFormat(it.date_of_birth)
            userProfile.postValue(it) }, { })
    }

    fun updateBirthDate(calendar: Calendar) {
        userProfile.value?.let {
            it.date_of_birth = DateUtil.getDatabaseFormat(calendar)
            repository.updateProfile(it, { updatedProfile ->
                updatedProfile.date_of_birth = DateUtil.getAppDateFormat(updatedProfile.date_of_birth)
                userProfile.postValue(updatedProfile)
            }, {
                errorMessage.postValue(R.string.error_updating_profile)
            })
        }
    }

    fun clearErrorMessage() { errorMessage.value = null }

    fun deleteUserData() { repository.deleteUser() }
}
