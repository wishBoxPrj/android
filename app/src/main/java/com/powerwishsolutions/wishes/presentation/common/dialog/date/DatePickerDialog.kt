package com.powerwishsolutions.wishes.presentation.common.dialog.date

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.utils.callback.DateSelection
import kotlinx.android.synthetic.main.fragment_date_picker_dialog.*
import java.lang.Exception
import java.util.*

class DatePickerDialog : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_date_picker_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        date_picker.maxDate = Calendar.getInstance().timeInMillis

        if (savedInstanceState != null) {
            val date = savedInstanceState.getSerializable("date") as Calendar
            date_picker.init(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), null)
        }

        date_apply.setOnClickListener {
            try {
                val listener = targetFragment as DateSelection
                val calendar = Calendar.getInstance()
                calendar.set(date_picker.year, date_picker.month, date_picker.dayOfMonth)
                listener.onDateSelected(calendar)
                dismiss()
            } catch (e: Exception) { }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setContentView(R.layout.fragment_date_picker_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onSaveInstanceState(outState: Bundle) {
        try {
            val calendar = Calendar.getInstance()
            calendar.set(date_picker.year, date_picker.month, date_picker.dayOfMonth)
            outState.putSerializable("date", calendar)
        } catch (e: Exception) { }
        super.onSaveInstanceState(outState)
    }
}
