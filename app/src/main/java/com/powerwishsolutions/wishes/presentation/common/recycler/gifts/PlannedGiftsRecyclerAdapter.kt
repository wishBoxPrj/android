package com.powerwishsolutions.wishes.presentation.common.recycler.gifts

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.presentation.common.view.SvgRatingBar
import com.shehabic.droppy.DroppyMenuPopup
import com.shehabic.droppy.animations.DroppyFadeInAnimation
import kotlinx.android.synthetic.main.item_gift.view.*

class PlannedGiftsRecyclerAdapter(
    private val context: Context?,
    private val list: ArrayList<Wish>,
    val listener: (Int) -> Unit,
    val longTapListener: (id: Int, position: Int) -> Unit,
    val emptyListener: (empty: Boolean) -> Unit
) : RecyclerView.Adapter<PlannedGiftsRecyclerAdapter.PlannedGiftsViewHolder>() {

    init { emptyListener(list.size == 0) }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): PlannedGiftsViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val itemView = inflater.inflate(R.layout.item_gift, viewGroup, false)
        return PlannedGiftsViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: PlannedGiftsViewHolder, position: Int) {
        holder.name.text = list[position].name

        if (list[position].comment.isNullOrEmpty()) {
            holder.owner.text = Html.fromHtml(context?.getString(
                R.string.item_for_name_without_comment,
                list[position].owner?.nickname)
            )
        } else {
            holder.owner.text = Html.fromHtml(context?.getString(
                R.string.item_for_name,
                list[position].owner?.nickname,
                list[position].comment)
            )
        }

        list[position].priority?.toFloat()?.let { holder.priority.rating = it }
        holder.itemView.setOnClickListener { listener(position) }

        val dropDownBuilder = DroppyMenuPopup.Builder(context, holder.itemView)
        val dropDown = dropDownBuilder.fromMenu(R.menu.gift_dropdown)
            .triggerOnAnchorClick(false)
            .setOnClick { _, id ->
                longTapListener(id, position)
            }
            .setPopupAnimation(DroppyFadeInAnimation())
            .build()

        holder.itemView.setOnClickListener { dropDown.show() }
    }

    class PlannedGiftsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.item_friend_wish_name
        val owner: TextView = itemView.item_friend_wish_comment
        val priority: SvgRatingBar = itemView.item_gift_priority
    }

    fun remove(gift: Wish) {
        val i = list.indexOf(gift)
        list.remove(gift)
        notifyItemRemoved(i)
        notifyItemRangeChanged(i, list.size)
        emptyListener(list.size == 0)
    }

    fun add(gift: Wish) {
        if (!list.contains(gift)) {
            list.add(0, gift)
            notifyItemInserted(0)
            notifyItemRangeChanged(1, list.size)
            emptyListener(list.size == 0)
        }
    }
}
