package com.powerwishsolutions.wishes.presentation.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.presentation.common.pager.MainFragmentAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = { item: MenuItem ->
        when (item.itemId) {
            R.id.navigation_friends -> main_view_pager.currentItem = 0
            R.id.navigation_plan -> main_view_pager.currentItem = 1
            R.id.navigation_wishlist -> main_view_pager.currentItem = 2
        }
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        main_view_pager.adapter = MainFragmentAdapter(supportFragmentManager)
        main_view_pager.offscreenPageLimit = 3
        main_navigation_view.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
