package com.powerwishsolutions.wishes.presentation.common.pager

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.presentation.friends.manage.requests.FriendRequestsFragment
import com.powerwishsolutions.wishes.presentation.friends.manage.search.SearchFriendsFragment

class ManageFriendsFragmentAdapter(private val context: Context?, fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragmentsCount = 2
    private val searchFriendsFragment = SearchFriendsFragment()
    private val requestsFragment = FriendRequestsFragment()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> searchFriendsFragment
            1 -> requestsFragment
            else -> searchFriendsFragment
        }
    }

    override fun getCount(): Int {
        return fragmentsCount
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context?.getString(R.string.title_add)
            1 -> context?.getString(R.string.title_requests)
            else -> context?.getString(R.string.title_add)
        }
    }
}
