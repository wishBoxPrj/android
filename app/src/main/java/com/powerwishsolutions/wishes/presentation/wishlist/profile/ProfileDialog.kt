package com.powerwishsolutions.wishes.presentation.wishlist.profile

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.extensions.setAvatar
import com.powerwishsolutions.wishes.presentation.auth.AuthActivity
import com.powerwishsolutions.wishes.presentation.common.dialog.date.DatePickerDialog
import com.powerwishsolutions.wishes.utils.callback.DateSelection
import kotlinx.android.synthetic.main.fragment_profile_dialog.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class ProfileDialog : DialogFragment(), DateSelection {

    private val vm: ProfileViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (vm.userInfo.value == null) vm.getUserInfo()
        if (vm.userProfile.value == null) vm.getUserProfile()

        vm.userInfo.observe(this, Observer {
            profile_name.text = getString(R.string.placeholder_name, it.first_name, it.last_name)
        })

        vm.userProfile.observe(this, Observer {
            profile_username.text = getString(R.string.placeholder_username, it.nickname)
            profile_birth_date.text = it.date_of_birth
            profile_avatar.setAvatar(context, profile_letter, it)

            profile_birth_date.setOnClickListener {
                fragmentManager?.let { manager ->
                    val dialog = DatePickerDialog()
                    dialog.setTargetFragment(this, 0)
                    dialog.show(manager, "date-picker")
                }
            }
        })

        vm.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(context, getString(it), Toast.LENGTH_SHORT).show()
                vm.clearErrorMessage()
            }
        })

        profile_logout.setOnClickListener {
            vm.deleteUserData()
            startActivity(Intent(context, AuthActivity::class.java))
            activity?.finish()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setContentView(R.layout.fragment_profile_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onDateSelected(calendar: Calendar) {
        vm.updateBirthDate(calendar)
    }
}
