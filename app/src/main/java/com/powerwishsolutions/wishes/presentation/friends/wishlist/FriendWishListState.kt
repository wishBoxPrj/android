package com.powerwishsolutions.wishes.presentation.friends.wishlist

import com.powerwishsolutions.wishes.domain.wishlist.model.Wish

sealed class FriendWishListState {
    object Initial : FriendWishListState()
    object Loading : FriendWishListState()
    object Error : FriendWishListState()
    class Success(val model: ArrayList<Wish>) : FriendWishListState()
}
