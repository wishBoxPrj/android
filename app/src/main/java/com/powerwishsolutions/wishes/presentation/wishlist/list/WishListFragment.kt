package com.powerwishsolutions.wishes.presentation.wishlist.list

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.extensions.*
import com.powerwishsolutions.wishes.presentation.common.recycler.wishes.UserWishListRecyclerAdapter
import com.powerwishsolutions.wishes.presentation.wishlist.create.WishCreateActivity
import com.powerwishsolutions.wishes.presentation.wishlist.edit.WishEditActivity
import com.powerwishsolutions.wishes.presentation.wishlist.profile.ProfileDialog
import com.powerwishsolutions.wishes.utils.common.RESULT_WISH_CREATED
import com.powerwishsolutions.wishes.utils.common.RESULT_WISH_DELETED
import com.powerwishsolutions.wishes.utils.common.RESULT_WISH_EDITED
import kotlinx.android.synthetic.main.fragment_wish_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class WishListFragment : Fragment() {

    private val vm: WishListViewModel by viewModel()
    private lateinit var adapter: UserWishListRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wish_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        wish_list_profile.setOnClickListener { ProfileDialog().show(childFragmentManager, "profile") }

        wish_list_recycler.setLinearOffsets(
            context,
            R.dimen.item_offset_14,
            R.dimen.item_offset_20,
            R.dimen.item_offset_14,
            R.dimen.item_offset_14
        )

        vm.state.observe(this, Observer {
            when (it) {
                is WishListState.Initial -> {
                    vm.getWishes(false)
                    vm.getProfileInfo()
                }
                is WishListState.Loading -> {
                    applyLoadingState(it.refresh, wish_list_recycler, wish_list_loader, wish_list_error, wish_list_refresh)
                }
                is WishListState.Success -> {
                    applySuccessState(wish_list_loader, wish_list_error, wish_list_refresh)

                    wish_list_recycler.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                    adapter = UserWishListRecyclerAdapter(it.model, { pos ->
                        startActivityForResult(Intent(context, WishEditActivity::class.java).apply {
                            putExtra("data", it.model[pos])
                        }, RESULT_WISH_EDITED)
                    }, { empty -> wish_list_empty.isVisible = empty })
                    wish_list_recycler.adapter = adapter

                    wish_list_add.setOnClickListener {
                        startActivityForResult(Intent(context, WishCreateActivity::class.java), RESULT_WISH_CREATED)
                    }
                }
                is WishListState.Error -> {
                    wish_list_empty.isVisible = false
                    applyErrorState(wish_list_recycler, wish_list_loader, wish_list_error, wish_list_refresh)
                    wish_list_error.setActionListener {
                        vm.getWishes(false)
                        vm.getProfileInfo()
                    }
                }
            }
        })

        vm.profileInfo.observe(this, Observer { wish_list_profile.setAvatar(context, wish_list_letter, it) })

        wish_list_refresh.setOnRefreshListener { vm.getWishes(true) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            RESULT_WISH_EDITED -> {
                if (data != null) adapter.edit(data.getParcelableExtra("data"))
            }
            RESULT_WISH_DELETED -> {
                if (data != null) adapter.remove(data.getParcelableExtra("data"))
            }
            RESULT_WISH_CREATED -> {
                if (data != null) adapter.add(data.getParcelableExtra("data"))
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
