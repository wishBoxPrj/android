package com.powerwishsolutions.wishes.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.powerwishsolutions.wishes.R
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.utils.common.GlideApp

@SuppressLint("SetTextI18n")
fun ImageView.setAvatar(context: Context?, letter: TextView?, profile: Profile?) {
    if (profile?.photo == null) {
        letter?.text = "${profile?.owner?.first_name?.substring(0, 1)}${profile?.owner?.last_name?.substring(0, 1)}"
    } else if (context != null) {
        GlideApp.with(context)
            .load(profile.photo)
            .override(100, 100)
            .placeholder(R.drawable.ic_avatar_placeholder)
            .error(R.drawable.ic_avatar_placeholder)
            .transform(CircleCrop())
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(exception: GlideException?, p1: Any?, p2: Target<Drawable>, p3: Boolean): Boolean {
                    // put letter in circle if picture load fails
                    letter?.text = "${profile.owner?.first_name?.substring(0, 1)}${profile.owner?.last_name?.substring(0, 1)}"
                    return false
                }
                override fun onResourceReady(p0: Drawable?, p1: Any?, p2: Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                    // remove letter from circle if picture successfully loaded
                    letter?.text = ""
                    return false
                }
            })
            .into(this)
    }
}
