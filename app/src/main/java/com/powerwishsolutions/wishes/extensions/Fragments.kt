package com.powerwishsolutions.wishes.extensions

import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.powerwishsolutions.wishes.presentation.common.view.ErrorView

fun Fragment.applyErrorState(
    rv: RecyclerView,
    loader: ProgressBar,
    errorView: ErrorView,
    refresh: SwipeRefreshLayout? = null
) {
    rv.adapter = null
    loader.isVisible = false
    errorView.isVisible = true
    refresh?.isRefreshing = false
    refresh?.isEnabled = false
}

fun Fragment.applyLoadingState(
    isRefresh: Boolean,
    rv: RecyclerView,
    loader: ProgressBar,
    errorView: ErrorView,
    refresh: SwipeRefreshLayout? = null
) {
    if (isRefresh) {
        refresh?.isRefreshing = true
    } else {
        rv.adapter = null
        loader.isVisible = true
        errorView.isVisible = false
        refresh?.isRefreshing = false
        refresh?.isEnabled = false
    }
}

fun Fragment.applySuccessState(
    loader: ProgressBar,
    errorView: ErrorView,
    refresh: SwipeRefreshLayout? = null
) {
    loader.isVisible = false
    errorView.isVisible = false
    refresh?.isRefreshing = false
    refresh?.isEnabled = true
}
