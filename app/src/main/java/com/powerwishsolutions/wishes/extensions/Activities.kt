package com.powerwishsolutions.wishes.extensions

import android.app.Activity
import android.content.Intent
import android.os.Parcelable
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.presentation.common.view.ErrorView

fun Activity.applyErrorState(
    rv: RecyclerView,
    loader: ProgressBar,
    errorView: ErrorView
) {
    rv.adapter = null
    loader.isVisible = false
    errorView.isVisible = true
}

fun Activity.applyLoadingState(
    rv: RecyclerView,
    loader: ProgressBar,
    errorView: ErrorView
) {
    rv.adapter = null
    loader.isVisible = true
    errorView.isVisible = false
}

fun Activity.applySuccessState(
    loader: ProgressBar,
    errorView: ErrorView
) {
    loader.isVisible = false
    errorView.isVisible = false
}

fun Activity.finishWithResultAndData(result: Int, data: Parcelable?) {
    val intent = Intent()
    intent.putExtra("data", data)
    setResult(result, intent)
    finish()
}
