package com.powerwishsolutions.wishes.extensions

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.powerwishsolutions.wishes.utils.decoration.LinearItemDecoration

fun RecyclerView.setLinearOffsets(context: Context?, vertical: Int, horizontal: Int, top: Int, bottom: Int) {
    context?.let {
        if (this.itemDecorationCount < 1) this.addItemDecoration(LinearItemDecoration(it, vertical, horizontal, top, bottom))
    }
}
