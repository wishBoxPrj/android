package com.powerwishsolutions.wishes.di.modules

import com.powerwishsolutions.wishes.domain.friends.repository.FriendsRepository
import com.powerwishsolutions.wishes.domain.friends.service.FriendService
import com.powerwishsolutions.wishes.presentation.friends.list.FriendsViewModel
import com.powerwishsolutions.wishes.presentation.friends.manage.ManageFriendsViewModel
import com.powerwishsolutions.wishes.presentation.friends.wishlist.FriendWishListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val friendsModule = module {
    viewModel { FriendsViewModel(get()) }
    viewModel { ManageFriendsViewModel(get()) }
    viewModel { FriendWishListViewModel(get(), get()) }
    single { FriendsRepository(get(), get()) }
    single { get<Retrofit>().create(FriendService::class.java) }
}
