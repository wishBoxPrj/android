package com.powerwishsolutions.wishes.di.modules

import com.powerwishsolutions.wishes.domain.wishlist.repository.WishRepository
import com.powerwishsolutions.wishes.domain.wishlist.service.WishService
import com.powerwishsolutions.wishes.presentation.common.dialog.category.CategoryChooseViewModel
import com.powerwishsolutions.wishes.presentation.plan.PlanViewModel
import com.powerwishsolutions.wishes.presentation.wishlist.create.WishCreateViewModel
import com.powerwishsolutions.wishes.presentation.wishlist.edit.WishEditViewModel
import com.powerwishsolutions.wishes.presentation.wishlist.list.WishListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val wishesModule = module {
    viewModel { WishListViewModel(get(), get()) }
    viewModel { WishCreateViewModel(get()) }
    viewModel { WishEditViewModel(get()) }
    viewModel { PlanViewModel(get()) }
    viewModel { CategoryChooseViewModel(get()) }
    single { WishRepository(get(), get()) }
    single { get<Retrofit>().create(WishService::class.java) }
}
