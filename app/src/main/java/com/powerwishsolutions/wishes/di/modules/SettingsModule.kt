package com.powerwishsolutions.wishes.di.modules

import android.content.Context
import com.powerwishsolutions.wishes.data.PreferencesRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val settingsModule = module {
    single { PreferencesRepository(get()) }
    single { androidApplication().getSharedPreferences("userPrefs", Context.MODE_PRIVATE) }
}
