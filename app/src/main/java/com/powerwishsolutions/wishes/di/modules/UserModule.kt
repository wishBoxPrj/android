package com.powerwishsolutions.wishes.di.modules

import com.powerwishsolutions.wishes.domain.user.repository.UserRepository
import com.powerwishsolutions.wishes.domain.user.service.UserService
import com.powerwishsolutions.wishes.presentation.auth.AuthViewModel
import com.powerwishsolutions.wishes.presentation.wishlist.profile.ProfileViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val userModule = module {
    viewModel { AuthViewModel(get()) }
    viewModel { ProfileViewModel(get()) }
    single { UserRepository(get(), get()) }
    single { get<Retrofit>().create(UserService::class.java) }
}
