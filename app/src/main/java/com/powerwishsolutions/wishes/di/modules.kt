package com.powerwishsolutions.wishes.di

import com.powerwishsolutions.wishes.di.modules.*

val modules = mutableListOf(
    networkModule,
    settingsModule,
    userModule,
    friendsModule,
    wishesModule
)
