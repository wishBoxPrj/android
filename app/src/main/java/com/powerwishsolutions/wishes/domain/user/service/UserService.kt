package com.powerwishsolutions.wishes.domain.user.service

import com.powerwishsolutions.wishes.domain.user.model.AuthForm
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.user.model.User
import io.reactivex.Single
import retrofit2.http.*

interface UserService {

    @POST("login/social/token_user/")
    fun authorize(@Body form: AuthForm): Single<User>

    @PATCH("profiles/me")
    fun setProfile(@Header("Authorization") token: String?, @Body profile: Profile?): Single<Profile>

    @GET("profiles/me")
    fun getProfile(@Header("Authorization") token: String?): Single<Profile>
}
