package com.powerwishsolutions.wishes.domain.user.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    var id: Int?,
    var first_name: String?,
    var last_name: String?,
    var username: String?,
    var email: String?,
    var token: String? = null
) : Parcelable
