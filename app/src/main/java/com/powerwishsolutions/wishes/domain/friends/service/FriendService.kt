package com.powerwishsolutions.wishes.domain.friends.service

import com.powerwishsolutions.wishes.domain.user.model.Profile
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.*

interface FriendService {

    @GET("friendship/friends")
    fun getFriends(@Header("Authorization") token: String?): Single<ArrayList<Profile>>

    @HTTP(method = "DELETE", path = "friendship/delete", hasBody = true)
    fun deleteFriend(@Header("Authorization") token: String?, @Body profile: Profile): Single<Profile>

    @GET("profiles")
    fun searchProfiles(@Header("Authorization") token: String?, @Query("nickname") nickname: String?): Single<ArrayList<Profile>>

    @GET("friendship/requests")
    fun getRequests(@Header("Authorization") token: String?): Single<ArrayList<Profile>>

    @POST("friendship/add")
    fun sendRequest(@Header("Authorization") token: String?, @Body profile: Profile): Single<Profile>

    @POST("friendship/response/accept")
    fun acceptRequest(@Header("Authorization") token: String?, @Body profile: Profile): Single<ResponseBody>

    @POST("friendship/response/reject")
    fun denyRequest(@Header("Authorization") token: String?, @Body profile: Profile): Single<ResponseBody>
}
