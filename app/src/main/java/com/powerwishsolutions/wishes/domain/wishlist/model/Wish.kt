package com.powerwishsolutions.wishes.domain.wishlist.model

import android.os.Parcelable
import com.powerwishsolutions.wishes.domain.user.model.Profile
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Wish(
    val id: Int? = null,
    var owner: Profile? = null,
    var name: String?,
    var picture: String? = null,
    var comment: String?,
    var category: String?,
    var priority: Int?,
    var booked_by: Profile? = null
) : Parcelable
