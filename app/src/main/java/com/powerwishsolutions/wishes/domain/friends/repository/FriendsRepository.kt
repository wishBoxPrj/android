package com.powerwishsolutions.wishes.domain.friends.repository

import android.content.SharedPreferences
import com.powerwishsolutions.wishes.domain.friends.service.FriendService
import com.powerwishsolutions.wishes.domain.user.model.Profile
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FriendsRepository(private val service: FriendService, private val preferences: SharedPreferences) {

    fun getFriends(onSuccess: (ArrayList<Profile>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .getFriends(token())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun searchFriends(query: String, onSuccess: (ArrayList<Profile>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .searchProfiles(token(), query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun deleteFriend(friend: Profile, onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .deleteFriend(token(), friend)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun sendRequest(friend: Profile, onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .sendRequest(token(), friend)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun acceptRequest(friend: Profile, onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .acceptRequest(token(), friend)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ onSuccess(friend) }, onError)
    }

    fun denyRequest(friend: Profile, onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .denyRequest(token(), friend)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ onSuccess(friend) }, onError)
    }

    fun getIncomingRequests(onSuccess: (ArrayList<Profile>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .getRequests(token())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    private fun token(): String? { return preferences.getString("token", "") }
}
