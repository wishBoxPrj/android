package com.powerwishsolutions.wishes.domain.wishlist.model

import com.google.gson.annotations.SerializedName
import com.powerwishsolutions.wishes.R

enum class Categories(val value: Int) {
    @SerializedName("technics")
    TECHNICS(R.string.category_technics),

    @SerializedName("children_s_products")
    CHILDRENS_PRODUCTS(R.string.category_childrens),

    @SerializedName("auto")
    AUTO(R.string.category_auto),

    @SerializedName("household_products")
    HOUSE(R.string.category_home),

    @SerializedName("sports_and_recreation")
    SPORTS(R.string.category_sports),

    @SerializedName("cosmetics")
    COSMETICS(R.string.category_cosmetics),

    @SerializedName("food")
    FOOD(R.string.category_food),

    @SerializedName("wardrobe")
    WARDROBE(R.string.category_wardrobe),

    @SerializedName("hobby")
    HOBBY(R.string.category_hobby),

    @SerializedName("books")
    BOOKS(R.string.category_books),

    @SerializedName("accessories")
    ACCESSORIES(R.string.category_accessories),

    @SerializedName("undefined")
    UNDEFINED(R.string.category_undefined)
}
