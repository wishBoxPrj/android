package com.powerwishsolutions.wishes.domain.wishlist.repository

import android.content.SharedPreferences
import com.google.gson.Gson
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import com.powerwishsolutions.wishes.domain.wishlist.service.WishService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class WishRepository(private val service: WishService, private val preferences: SharedPreferences) {

    fun getWishes(onSuccess: (ArrayList<Wish>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .getWishes(token(), nickname())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun createWish(data: Wish?, onSuccess: (Wish) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .createWish(token(), data, nickname())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun editWish(data: Wish?, onSuccess: (Wish) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .editWish(token(), data, data?.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun deleteWish(data: Wish, onSuccess: (Wish) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .deleteWish(token(), data.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ onSuccess(data) }, onError)
    }

    fun getCategories(onSuccess: (ArrayList<String?>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .getCategories(token())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun getFriendWishList(nickname: String?, onSuccess: (ArrayList<Wish>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .getWishes(token(), nickname)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun bookWish(wish: Wish?, onSuccess: (Wish?) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .bookWish(token(), wish?.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onSuccess(wish?.apply { booked_by = profile() })
            }, onError)
    }

    fun getBookedWishes(onSuccess: (ArrayList<Wish>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .getBookedWishes(token())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun unbookWish(wish: Wish, onSuccess: (Wish) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return service
            .unbookWish(token(), wish.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ onSuccess(wish) }, onError)
    }

    private fun nickname(): String? { return preferences.getString("nickname", "") }

    private fun token(): String? { return preferences.getString("token", "") }

    private fun profile(): Profile? {
        return Gson().fromJson(preferences.getString("profile", ""), Profile::class.java)
    }
}
