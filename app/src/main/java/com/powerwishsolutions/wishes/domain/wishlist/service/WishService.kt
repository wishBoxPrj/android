package com.powerwishsolutions.wishes.domain.wishlist.service

import com.powerwishsolutions.wishes.domain.wishlist.model.Wish
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface WishService {

    @GET("/gifts/{nickname}")
    fun getWishes(
        @Header("Authorization") token: String?,
        @Path("nickname") nickname: String?
    ): Single<ArrayList<Wish>>

    @POST("/gifts/{nickname}")
    fun createWish(
        @Header("Authorization") token: String?,
        @Body wish: Wish?,
        @Path("nickname") nickname: String?
    ): Single<Wish>

    @DELETE("/gifts/{id}")
    fun deleteWish(
        @Header("Authorization") token: String?,
        @Path("id") id: Int?
    ): Single<Response<Void>>

    @PATCH("/gifts/{id}")
    fun editWish(
        @Header("Authorization") token: String?,
        @Body wish: Wish?,
        @Path("id") id: Int?
    ): Single<Wish>

    @GET("/gifts/booked/me")
    fun getBookedWishes(
        @Header("Authorization") token: String?
    ): Single<ArrayList<Wish>>

    @POST("/gifts/booked/{id}")
    fun bookWish(
        @Header("Authorization") token: String?,
        @Path("id") id: Int?
    ): Single<Response<Void>>

    @DELETE("/gifts/booked/{id}")
    fun unbookWish(
        @Header("Authorization") token: String?,
        @Path("id") id: Int?
    ): Single<Response<Void>>

    @GET("/gifts/categories")
    fun getCategories(
        @Header("Authorization") token: String?
    ): Single<ArrayList<String?>>
}
