package com.powerwishsolutions.wishes.domain.user.model

data class AuthForm(val code: String) {
    val provider: String = "vk-oauth2"
    val redirect_uri: String = "http://powerwishes.me/"
}
