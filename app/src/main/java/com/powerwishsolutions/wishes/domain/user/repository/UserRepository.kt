package com.powerwishsolutions.wishes.domain.user.repository

import android.content.SharedPreferences
import com.google.gson.Gson
import com.powerwishsolutions.wishes.domain.user.model.AuthForm
import com.powerwishsolutions.wishes.domain.user.model.Profile
import com.powerwishsolutions.wishes.domain.user.model.User
import com.powerwishsolutions.wishes.domain.user.service.UserService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class UserRepository(private val userService: UserService, private val preferences: SharedPreferences) {

    fun auth(form: AuthForm, onSuccess: (User) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return userService
            .authorize(form)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun getUser(onSuccess: (User) -> Unit, onError: (Throwable) -> Unit) {
        try {
            val user = Gson().fromJson(preferences.getString("user", ""), User::class.java)
            onSuccess(user)
        } catch (e: Exception) {
            onError(e)
        }
    }

    fun getProfile(onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return getProfile(preferences.getString("token", ""), onSuccess, onError)
    }

    fun getProfile(token: String?, onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return userService
            .getProfile(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun updateProfile(profile: Profile?, onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return setProfile(preferences.getString("token", ""), profile, onSuccess, onError)
    }

    fun setProfile(token: String?, profile: Profile?, onSuccess: (Profile) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return userService
            .setProfile(token, profile)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    fun saveUser(user: User?, profile: Profile?) {
        if (user != null) {
            preferences.edit().putString("token", "Token ${user.token}")
                .putString("nickname", profile?.nickname)
                .putString("profile", Gson().toJson(profile))
                .putString("user", Gson().toJson(user))
                .apply()
        }
    }

    fun deleteUser() {
        preferences.edit()
            .remove("user")
            .remove("nickname")
            .remove("profile")
            .remove("token")
            .apply()
    }
}
