package com.powerwishsolutions.wishes.domain.user.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Profile(
    var nickname: String?,
    var date_of_birth: String?,
    var photo: String?,
    var owner: User? = null
) : Parcelable
