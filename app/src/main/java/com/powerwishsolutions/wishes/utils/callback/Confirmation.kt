package com.powerwishsolutions.wishes.utils.callback

interface Confirmation {
    fun onConfirm()
}
