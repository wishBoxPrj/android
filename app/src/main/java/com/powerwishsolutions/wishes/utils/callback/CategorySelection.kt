package com.powerwishsolutions.wishes.utils.callback

interface CategorySelection {
    fun onCategorySelected(category: String?)
}
