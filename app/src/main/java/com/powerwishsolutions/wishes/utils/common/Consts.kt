package com.powerwishsolutions.wishes.utils.common

const val RESULT_WISH_DELETED = 41
const val RESULT_WISH_EDITED = 42
const val RESULT_WISH_CREATED = 43

const val RESULT_FRIENDS_UPDATED = 43

const val ACTION_ACCEPT_REQUEST = 60
const val ACTION_DENY_REQUEST = 61
