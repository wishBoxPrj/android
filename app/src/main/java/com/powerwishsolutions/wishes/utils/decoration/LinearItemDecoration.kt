package com.powerwishsolutions.wishes.utils.decoration

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView

class LinearItemDecoration(
    private val verticalOffset: Int,
    private val horizontalOffset: Int,
    private val topOffset: Int,
    private val bottomOffset: Int
) : RecyclerView.ItemDecoration() {

    constructor(context: Context, @DimenRes verticalOffset: Int, @DimenRes horizontalOffset: Int, @DimenRes topOffset: Int, @DimenRes bottomOffset: Int) :
            this(
                context.resources.getDimensionPixelSize(verticalOffset),
                context.resources.getDimensionPixelSize(horizontalOffset),
                context.resources.getDimensionPixelSize(topOffset),
                context.resources.getDimensionPixelSize(bottomOffset))

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        when (parent.getChildLayoutPosition(view)) {
            0 -> outRect.set(horizontalOffset, topOffset, horizontalOffset, verticalOffset)
            state.itemCount - 1 -> outRect.set(horizontalOffset, 0, horizontalOffset, bottomOffset)
            else -> outRect.set(horizontalOffset, 0, horizontalOffset, verticalOffset)
        }
    }
}
