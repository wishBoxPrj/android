package com.powerwishsolutions.wishes.utils.callback

import java.util.*

interface DateSelection {
    fun onDateSelected(calendar: Calendar)
}
