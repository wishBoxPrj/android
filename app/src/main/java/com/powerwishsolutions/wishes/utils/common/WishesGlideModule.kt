package com.powerwishsolutions.wishes.utils.common

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class WishesGlideModule : AppGlideModule()
