package com.powerwishsolutions.wishes.utils.common

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    fun getDatabaseFormat(calendar: Calendar): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return dateFormat.format(calendar.time)
    }

    fun getAppDateFormat(date: String?): String? {
        return try {
            val dbDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val appDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
            val parsedDate = dbDateFormat.parse(date)
            appDateFormat.format(parsedDate)
        } catch (e: Exception) {
            null
        }
    }
}
