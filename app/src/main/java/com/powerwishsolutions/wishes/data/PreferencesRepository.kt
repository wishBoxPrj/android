package com.powerwishsolutions.wishes.data

import android.content.Context

class PreferencesRepository(context: Context) {

    private var preferences = context.getSharedPreferences("userPrefs", Context.MODE_PRIVATE)

    fun getToken(): String { return preferences.getString("token", "")!! }
}
