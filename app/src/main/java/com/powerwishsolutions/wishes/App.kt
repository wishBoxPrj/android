package com.powerwishsolutions.wishes

import android.app.Application
import com.powerwishsolutions.wishes.di.modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(modules)
        }
    }
}
